<?php
return [
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone'=>'Asia/Baku',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat'=>'d MMMM yyyy'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
