<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/17/16
 * Time: 9:29 PM
 */
namespace console\controllers;
use frontend\models\Authors;
use yii\console\Controller;

class PushController extends Controller{
    public function actionFake(){
        $authors = ['Сергей Кузнецов','Андрей Буторин','Сергей Антонов','Александр Шакилов','Руслан Мельников','Сергей Палий','Сергей Москвин','Андрей Гребенщиков','Игорь Вардунас'];
        foreach($authors as $author){
            $authorName = explode(' ',$author);
            $newAuthor = new Authors();
            $newAuthor->firstname = $authorName[0];
            $newAuthor->lastname = $authorName[1];
            $newAuthor->save(false);
        }
    }
}
