<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Books;
use frontend\models\BooksSearch;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create', 'update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Books models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Books model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            if(!empty($id)){
                $view = null;
                if(!Yii::$app->user->isGuest){
                    $model = $this->findModel($id);
                    $view = $this->renderPartial('view',['model'=>$model]);
                }
                return $view;
            }
        }
        return false;
    }

    /**
     * Creates a new Books model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Books();
        if ($model->load(Yii::$app->request->post())) {
            $this->saveAndResizeImage($model);
            $model->date_create = date('Y-m-d');
            $model->save();
            return $this->redirect('/');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Books model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$token)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $this->saveAndResizeImage($model);
            $model->date_update = date('Y-m-d');
            $model->save();

            return $this->redirect(base64_decode($token));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Books model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function saveAndResizeImage($model){
        $model->image = UploadedFile::getInstance($model, 'image');
        if(!empty($model->image)) {
            if(!file_exists(Yii::getAlias('@frontend/web/uploads'))){
                mkdir(Yii::getAlias('@frontend/web/uploads'),777);
            }
            $imageName = $model->image->baseName . '.' . $model->image->extension;
            $model->image->saveAs(Yii::getAlias('@frontend/web/uploads/'.$imageName));
            $model->preview = $imageName;
            $bigImage = Image::thumbnail(Yii::getAlias('@frontend/web/uploads/'.$imageName),800,600);
            if($bigImage->save(Yii::getAlias('@frontend/web/uploads/'.$imageName),['quality'=>100])){
                $smallImage = Image::thumbnail(Yii::getAlias('@frontend/web/uploads/'.$imageName),100,100);
                $smallImage->save(Yii::getAlias('@frontend/web/uploads/small-'.$imageName),['quality'=>100]);
            }
        }
    }
    /**
     * Finds the Books model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Books the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Books::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
