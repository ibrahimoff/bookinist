<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $image;
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date', 'author_id'], 'required'],
            [['date_create', 'preview','date_update'], 'safe'],
            [['author_id'], 'integer'],
            [['image'],'file'],
            [['name', 'preview'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название книги',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'image' => 'Обложка',
            'preview' => 'Обложка',
            'author' => 'Автор',
            'date' => 'Дата выхода книги',
            'author_id' => 'Автор',
            'date_from'=>'Дата выхода от',
            'date_to'=>'Дата выхода до',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getPreview(){
        return '<a href="'.Yii::getAlias('@web/uploads/'.$this->preview).'" data-lity>'.Html::img(Yii::getAlias('@web/uploads/small-'.$this->preview)).'</a>';
    }
    public function getAuthorsArray(){
        $authors = Authors::find()->indexBy('id')->all();
        $authorsArray = [];
        foreach($authors as $key=>$value){
            $authorsArray[$key] = $value->firstname.' '.$value->lastname;
        }
        return $authorsArray;
    }
    public function formatPublishDate(){
        return date("d F Y", strtotime($this->date));
    }


    public function relativeDays($date){
            $time = strtotime($date);
            $today = strtotime(date('M j, Y'));

            $reldays = ($time - $today)/86400;

            if ($reldays >= 0 && $reldays < 1) {

                return 'Сегодня';

            }else if ($reldays >= -1 && $reldays < 0) {

                return 'Вчера';

            }
            if (abs($reldays) < 182) {
                return date('d F Y',$time ? $time : time());

            } else {

                return date('d F Y',$time ? $time : time());

            }

    }
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }
}
