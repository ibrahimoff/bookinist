<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Books;

/**
 * BooksSearch represents the model behind the search form about `frontend\models\Books`.
 */
class BooksSearch extends Books
{
    /**
     * @inheritdoc
     */
    public $date_from;
    public $date_to;
    public $author;
    public function rules()
    {
        return [
            [['name', 'date_from','date_to','author'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        };

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['=','author_id',$this->author])
            ->andFilterWhere(['>=','date',$this->date_from])
            ->andFilterWhere(['<=','date',$this->date_to]);
        return $dataProvider;
    }
}
