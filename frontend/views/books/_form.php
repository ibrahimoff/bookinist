<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Books */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord){
    $imageUploadPluginOptions = [
        'showUpload' => false,
        'showRemove' => false,
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']]
    ];
}else{
    $imageUploadPluginOptions = [
        'showUpload' => false,
        'showRemove' => false,
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']],
        'initialPreview'=>[
            Html::img(Yii::getAlias('@web/uploads/'.$model->preview), ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
        ],
        'initialCaption'=>$model->preview,
        'overwriteInitial'=>true
    ];
}


?>
<div class="books-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => $imageUploadPluginOptions
    ]);?>
    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::classname(), [
        'options' => ['placeholder' => 'Дата выхода книги'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
        ]
    ]);?>

    <?= $form->field($model, 'author_id')->dropDownList($model->getAuthorsArray(),['prompt'=>'Выберите автора']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
