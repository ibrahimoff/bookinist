<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'author')->dropDownList($model->getAuthorsArray(),['prompt'=>'Выберите автора']) ?>

        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'date_from')->widget(\kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Дата выхода книги от ...'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'date_to')->widget(\kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Дата выхода книги до ...'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]); ?>

        </div>
    </div>






    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'author_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
