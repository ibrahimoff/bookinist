<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bookinist';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(!Yii::$app->user->isGuest):?>
            <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','visible'=>false],

            'id',
            'name',
            [
                'attribute'=>'preview',
                'label'=>'Предпросмотр',
                'format'=>'raw',
                'value'=>function($data){
                    return $data->getPreview();
                }
            ],
            [
                'attribute'=>'author_id',
                'label'=>'Автор',
                'value'=>function($data){
                    return $data->author->getFullName();
                }
            ],

            [
                'attribute'=>'date',
                'label'=>'Дата выхода книги',
                'value'=>function($data){
                    return $data->formatPublishDate();
                }
            ],
            [
                'attribute'=>'date_create',
                'label'=>'Дата создания',
                'value'=>function($data){
                    return $data->relativeDays($data->date_create);
                }
            ],


            ['class' => 'yii\grid\ActionColumn','visible'=>!Yii::$app->user->isGuest,
                'template' => !Yii::$app->user->isGuest? '{update}{view}{delete}':'',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                    return  Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url.'&token='.base64_encode(Yii::$app->request->url),['target'=>"_blank"]);
                    },
                    'view' => function ($url, $model, $key) {
                        return  Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#',['class'=>'view_book','data-id'=>$model->id,
                            "data-toggle"=>"modal","data-target"=>"#viewBookModal"]);
                    }
                ]

            ],
        ],
    ]); ?>

</div>

<div class="modal fade" id="viewBookModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="appendBookInfo">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
