<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Books */

?>
<div class="books-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute'=>'date_create',
                'value'=>$model->relativeDays($model->date_create),
            ],
            [
                'attribute'=>'date_update',
                'value'=>$model->relativeDays($model->date_update),
            ],
            [
                'attribute'=>'preview',
                'format'=>'html',
                'value'=>Html::img(Yii::getAlias('@web/uploads/small-'.$model->preview)),
            ],
            [
                'attribute'=>'date',
                'value'=>$model->formatPublishDate(),
            ],
            [
                'attribute'=>'author_id',
                'label'=>'Автор',
                'value'=>$model->author->getFullName(),
            ],
        ],
    ]) ?>

</div>
