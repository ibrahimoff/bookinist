$(document).on('click','.view_book',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var container = $('#appendBookInfo');
    var buttons = '<div class="text-center">' +
        '<div>Вы вошли на сайт как гость..</div>'+
        '<div><a href="/site/login" class="btn btn-success btn-lg" style="width: 200px;">Войти</a></div>'+
        '<div>или</div>'+
        '<div><a href="/site/signup" class="btn btn-info btn-lg"  style="width: 200px;">Зарегистрироваться</a></div>' +
        '</div>';
    $.ajax({
        url:'/books/view?id='+id,
        method:'get'
    }).success(function(response){
        if(response != ''){
            container.html(response)
        }else{
            container.html(buttons)
        }
    }).error(function(err){
        console.error(err)
    });
})